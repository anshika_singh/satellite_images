# -*- coding: utf-8 -*-
"""
Created on Mon Sep 10 17:21:37 2018

@author: admin1
"""

import numpy as np
import os, sys

from osgeo import gdal
from sklearn import metrics
from sklearn.utils import shuffle, class_weight
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

from keras.models import Model
from keras.optimizers import Adam

## Keras related modules
from keras.models import Sequential, save_model, load_model
from keras.layers import Dense, Dropout, BatchNormalization
from keras import optimizers, regularizers, initializers
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.utils import to_categorical

import matplotlib.pyplot as plt
import seaborn as sns
plt.style.use(['default'])
params = {
    'text.latex.preamble': ['\\usepackage{gensymb}'],
    'image.origin': 'lower',
    'image.interpolation': 'nearest',
    'image.cmap': 'gray',
    'axes.grid': False,
    'savefig.dpi': 150,  # to adjust notebook inline plot size
    'axes.labelsize': 12, # fontsize for x and y labels (was 10)
    'axes.titlesize': 12,
    'font.size': 12, # was 10
    'legend.fontsize': 12, # was 10
    'xtick.labelsize': 12,
    'ytick.labelsize': 12,
    'text.usetex': True,
    'figure.facecolor':'white',
    'font.family': 'serif',
}
plt.rcParams.update(params)

def create_mask_from_vector(vector_data_path, cols, rows, geo_transform,
                            projection, target_value=1):
    """Rasterize the given vector (wrapper for gdal.RasterizeLayer)."""
    data_source = gdal.OpenEx(vector_data_path, gdal.OF_VECTOR)
    layer = data_source.GetLayer(0)
    driver = gdal.GetDriverByName('MEM')  # In memory dataset
    target_ds = driver.Create('', cols, rows, 1, gdal.GDT_UInt16)
    target_ds.SetGeoTransform(geo_transform)
    target_ds.SetProjection(projection)
    gdal.RasterizeLayer(target_ds, [1], layer, burn_values=[target_value])
    return target_ds


def vectors_to_raster(file_paths, rows, cols, geo_transform, projection):
    """Rasterize the vectors in the given directory in a single image."""
    labeled_pixels = np.zeros((rows, cols))
    for i, path in enumerate(file_paths):
        label = i+1
        ds = create_mask_from_vector(path, cols, rows, geo_transform,
                                     projection, target_value=label)
        band = ds.GetRasterBand(1)
        labeled_pixels += band.ReadAsArray()
        ds = None
    return labeled_pixels

def write_geotiff(fname, data, geo_transform, projection):
    """Create a GeoTIFF file with the given data."""
    driver = gdal.GetDriverByName('GTiff')
    rows, cols = data.shape
    dataset = driver.Create(fname, cols, rows, 1, gdal.GDT_Byte)
    dataset.SetGeoTransform(geo_transform)
    dataset.SetProjection(projection)
    band = dataset.GetRasterBand(1)
    band.WriteArray(data)
    dataset = None  # Close the file
    
raster_data_path = "images/tiles/tile_1000_4000.tif"
output_fname = ".classification.tif"
shapefile_path = "./shape_files/splits/"
# validation_data_path = "data/train/"
raster_dataset = gdal.Open(raster_data_path, gdal.GA_ReadOnly)
geo_transform = raster_dataset.GetGeoTransform()
proj = raster_dataset.GetProjectionRef()
bands_data = []
for b in range(1, raster_dataset.RasterCount+1):
    band = raster_dataset.GetRasterBand(b)
    bands_data.append(band.ReadAsArray())

bands_data = np.dstack(bands_data)
rows, cols, n_bands = bands_data.shape
print(rows, cols, n_bands)
files = [f for f in os.listdir(shapefile_path) if f.endswith('.shp')]
classes = [f.split('.')[0] for f in files]
shapefiles = [os.path.join(shapefile_path, f)
              for f in files if f.endswith('.shp')]

data_source = gdal.OpenEx(shapefile_path, gdal.OF_VECTOR)
layer = data_source.GetLayer(0)
labeled_pixels = vectors_to_raster(shapefiles, rows, cols, geo_transform, proj)
is_train = np.nonzero(labeled_pixels)

print(np.shape(labeled_pixels), np.shape(bands_data))
xtrain = np.delete(bands_data, 10, 2)
xtrain = np.delete(xtrain, 9, 2)
n_samples = rows*cols
flat_pixels = xtrain.reshape((n_samples, 9))
ytrain = labeled_pixels.reshape((n_samples, 1))
print(np.shape(xtrain))


# Class distribution
from scipy.stats import itemfreq
print(itemfreq(ytrain).astype(int))
ytrain[ytrain == 5 ] = 0
label_count = itemfreq(ytrain).astype(int)[:,1]
class_name = ['Low land', 'Elevated area', 'Water bodies']
index = np.arange(len(class_name))
# plt.bar(class_name, count)
sns.barplot(x = class_name, y = label_count, linewidth = 0.5, errcolor = ".2", edgecolor = ".2")
plt.xlabel('Class', fontsize=15)
plt.ylabel('Number of data', fontsize=15)
plt.xticks(index, class_name, fontsize=15, rotation = 'vertical')
plt.title('Class distribution')
plt.show()

# Preprocessing
mms = StandardScaler()
xtrain_scaled = mms.fit_transform(flat_pixels)

# Random Forest Classification
classifier = RandomForestClassifier(n_jobs=-1)
classifier.fit(xtrain_scaled, ytrain)
result = classifier.predict(xtrain_scaled)
classification = result.reshape((rows, cols))
def discrete_cmap(N, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))
    cmap_name = base.name + str(N)
    return base.from_list(cmap_name, color_list, N)
f = plt.figure(figsize=(15, 10))
plt.subplot(1, 3, 1)
plt.imshow(bands_data[:, :, 6])
plt.title('Original Image')

plt.subplot(1, 3, 2)
im = plt.imshow(labeled_pixels.astype(np.float64), cmap = discrete_cmap(3, 'rainbow'))
plt.title('Vector Mask')
cbar = plt.colorbar(im, ticks=[0, 1, 2, 3], fraction=0.046, pad=0.04)
cbar.ax.set_yticklabels(class_name)

plt.subplot(1, 3, 3)
im = plt.imshow(classification, cmap = discrete_cmap(3, 'rainbow'))
plt.title('Classified Image')
cbar = plt.colorbar(im, ticks=[0, 1, 2, 3], fraction=0.046, pad=0.04)
cbar.ax.set_yticklabels(class_name)
plt.tight_layout()
plt.show()

test_data_path = "images/tiles/tile_1000_3000.tif"
test_raster = gdal.Open(test_data_path, gdal.GA_ReadOnly)
geo_transform = test_raster.GetGeoTransform()
proj = test_raster.GetProjectionRef()
test_bands_data = []
for b in range(1, test_raster.RasterCount+1):
    band = test_raster.GetRasterBand(b)
    test_bands_data.append(band.ReadAsArray())

test_bands_data = np.dstack(test_bands_data)
test_rows, test_cols, test_n_bands = test_bands_data.shape

np.shape(test_bands_data)
xtest = np.delete(test_bands_data, 10, 2)
xtest = np.delete(xtest, 9, 2)

np.shape(xtest)
test_flat_pixels = xtest.reshape((n_samples, 9))
test_mms = StandardScaler()
xtest_scaled = test_mms.fit_transform(test_flat_pixels)
test_result = classifier.predict(xtest_scaled)
test_classification = test_result.reshape((rows, cols))
f = plt.figure(figsize=(8, 6))

plt.subplot(1, 2, 1)
im = plt.imshow(test_bands_data[:, :, 6])
plt.title('Original Image')
plt.colorbar(im,fraction=0.046, pad=0.04)

plt.subplot(1, 2, 2)
im = plt.imshow(test_classification, cmap=discrete_cmap(3, 'rainbow'))
plt.title('Classified Image')
cbar = plt.colorbar(im, ticks=[0, 1, 2, 3], fraction=0.046, pad=0.04)
cbar.ax.set_yticklabels(class_name)
plt.tight_layout()
plt.show()

# Deep Neural Network
len(ytrain)
weights = len(ytrain) / (3 * np.unique(ytrain))
aa = class_weight.compute_class_weight('balanced', 
                                       np.unique(ytrain.astype(int)), 
                                       ytrain.astype(int))
CLASS_WEIGHT = {0 : aa[0] , 1: aa[1], 2: aa[2]}
# Parameter Tuning
HIDDEN_LAYER_SIZE = 12
L2_REGULARIZER = 5
LEARNING_RATE = 0.0001
TRAINING_EPOCHS = 15
BATCH_SIZE = 32
VALIDATION_SPLIT = 0.30
# CLASS_WEIGHT = CLASS_WEIGHT

# Building model
model = Sequential()
model.add(Dense(16, activation = 'relu',
                kernel_initializer = initializers.RandomNormal(mean = 0.0, stddev = 1, seed = 100),
                bias_initializer = 'Ones',
                input_shape = (9,),
                name = 'layer_1'))

model.add(Dense(8, activation = 'relu',
                kernel_initializer = initializers.RandomNormal(mean = 0.0, stddev = 1, seed = 100),
                bias_initializer = 'Ones',
                name = 'layer_2'))

model.add(Dense(4, activation = 'relu',
                kernel_initializer = initializers.RandomNormal(mean = 0.0, stddev = 1, seed = 100),
                bias_initializer = 'Ones',
                name = 'layer_3'))


adm_optz = optimizers.Adam(lr = LEARNING_RATE)
model.add(Dense(4, activation = "softmax", name = 'Output_layer'))

model.compile(optimizer = adm_optz, loss = 'categorical_crossentropy', 
              metrics = ['accuracy'])

# Model Training
ytrain_on_hot = to_categorical(ytrain, num_classes=4)

early_stopping = EarlyStopping(monitor = 'val_loss', patience = 20)

history = model.fit(xtrain_scaled, ytrain_on_hot, 
                    batch_size = BATCH_SIZE, 
                    epochs = TRAINING_EPOCHS,
#                     class_weight = CLASS_WEIGHT,
#                     callbacks = callback_list,
                    validation_split = VALIDATION_SPLIT, 
                    verbose = 1)
# Prediction
ypred = model.predict(xtest_scaled)
nnypred = np.argmax(ypred, axis = 1)
classification = nnypred.reshape((rows, cols))
f = plt.figure(figsize=(8, 6))

plt.subplot(1, 2, 1)
im = plt.imshow(test_bands_data[:, :, 6])
plt.title('Original Image')
# plt.colorbar(im,fraction=0.046, pad=0.04)

plt.subplot(1, 2, 2)
im = plt.imshow(classification, cmap=discrete_cmap(3, 'rainbow'))
plt.title('Classified Image')
cbar = plt.colorbar(im, ticks=[0, 1, 2, 3], fraction=0.046, pad=0.04)
cbar.ax.set_yticklabels(class_name)
plt.tight_layout()
plt.show()

# Training Classification report
print('Accuracy of the model: {}\n'.format(accuracy_score(ytrain, nnypred)))
print('Classification report: \n{}\n'.format(classification_report(ytrain, nnypred)))
print('Confusion matrix: \n{}\n'.format(confusion_matrix(ytrain, nnypred)))
