In this project, I have developed a machine learning model to detect geological features from Landsat 8 images. 
In this project, I aim to develop machine learning models that can detect different geologic features especially faults from 
satellite images. I have used a deep neural network and random forest algorithms to identify features from Landsat 8 images. 
The model is powerful that it can even detect recently developed river bar, and sharp fault bends. 
I am still working on the model to make it a better predictor. 